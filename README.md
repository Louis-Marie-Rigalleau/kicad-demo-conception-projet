# 📢 KiCad Demo Conception Projet

Présentation de la mise en oeuvre d'un projet avec [KiCad](https://www.kicad.org/).

Pour ce projet, nous avons utilisé la "[Version stable](https://www.kicad.org/download/windows/)" de KiCad pour Windows, qui est la version 7.0.7.

## 💡Présentation du projet

Nous vous proposons l'étude d'un stylo testeur de niveau logique.

Celui-ci alimenté par la carte électronique sous test, permettra le débogage des circuits numériques à base de microcontrôleur.

Voir la définition d'[une sortie à trois états](https://fr.wikipedia.org/wiki/Sortie_%C3%A0_trois_%C3%A9tats).

<!-- ![vue-de-presentation-du-stylo-logique.png](images/vue-de-presentation-du-stylo-logique.png) -->
<!-- ![mise-en-oeuvre-du-stylo-logique](images/mise-en-oeuvre-du-stylo-logique.png) -->

|                                 Vue d'ensemble du stylo logique                                  |              Vue en situation avec une carte sous test               |
| :----------------------------------------------------------------------------------------------: | :------------------------------------------------------------------: |
|            <img src="images/vue-de-presentation-du-stylo-logique.png" width="450" />             | <img src="images/mise-en-oeuvre-du-stylo-logique.png" width="450" /> |
| Source : [Logic Tester Pen sur AliExpress](https://fr.aliexpress.com/item/1005004890722065.html) |                                                                      |

---

## 📌 Les fichiers à produire pour le développement d'une carte électronique

- [x] Fichier de conception (Etude du cahier des charges)
- [ ] Fichier de licence
- [x] Fichier de schématique
- [x] Fichier de routage
- [x] Fichier de sérigraphie
- [x] Fichier de nomenclature (Liste des composants = "BOM" : Bill Of Material)
- [ ] Fichier de fabrication Gerber (exporter les couches : Cu, Paste, Mask, Silkscreen)
- [ ] Fichier de perçage
- [ ] Fichier de placement
- [ ] Fichier de Pick and Place
- [ ] Fichier de documentation 📖
- [ ] Fichier de test
- [ ] Fichier de rapport
<!-- - [ ] Fichier de netlist pour un partage avec d'autres lo CAO électronique -->

---

## 📝 Cahier des charges pour la conception du stylo testeur de niveau logique

Taille de la carte électronique : 210 x 18 mm

### 📝 Fonctionnalités attendues

- Alimentation par la carte électronique sous test à l'aide de cables Dupont
- Niveaux logiques mesurable de 3V3 à 5V (VCC : Alimentation d'entrée maximale 5 V)
- Affichage des niveaux logiques par des LEDs, affichage à trois états: haute, basse, haute impédance (déconnecté)
- Niveau haut et bas lorsque VCC = 5V :
  - lumière rouge (niveau haut), lorsque la tension est au-dessus de 2.2 V
  - lumière verte (niveau bas), lorsque la tension est au-dessous de 0.5 V
  - lumière bleue (état haute impédance), entre 0.5 et 2.2 V ou déconnecté du montage de test

### 📝 Contraintes

**TTL :**

* Etat logique "1" (LED H) > 2,7 ± 0,2 V
* Etat logique "0" (LED L) < 0,5 ± 0,2 V

**CMOS :**

* Etat logique "1" (LED H) > 70% Vcc ± 10%
* Etat logique "0" (LED L) < 30% Vcc ± 10%

![TTL-CMOS](images/niveaux-ttl-cmos.png)

Utilisation du composant [LM339, quadruple comparateur différentiel](https://www.ti.com/product/LM339).

### Simulation pour le choix des valeurs des résistances de comparaison des seuils

Voir [SPICE Simulation](https://www.kicad.org/discover/spice/) avec KiCad.

Génération d'un signal triangulaire de 1 kHz et de 5 Volts d'amplitude, réalisé avec un générateur de tension impulsionnel.

VPULSE définition ${Sim.Params} :

y1=Voff y2=Von td=Tdelay tr=Trise tf=Tfall tw=Twidth per=Tperiod

`y1=0 y2=5 td=0 tr=500u tf=500u tw=.1n per=1m`

Ici sur 2 périodes de 1 ms, soit une simulation sur 2 ms :

`.tran 1m 2m`

![Simulation](images/sim-transitoire-2p-triang-1khz.png)

Visualisation des seuils de comparaison :

![Simulation avec seuils](images/sim-transitoire-2p-triang-1khz-visu-seuils.png)

---

## 📝 Schématique

[Schema électronique](/FLE-V1.0/docs/FLE-V1.0-Schematique.pdf) ([+ simulation](/FLE-V1.0/docs/FLE-V1.0-Schematique+simu.pdf)) :

![Logic Probe](images/schema-sonde-logique.png)

Source : https://www.niguru.com/2018/06/logic-probe-yang-dapat-menampilkan.html

---

## 📝 Routage

<!-- ![FLE-V1.0-Top](images/FLE-V1.0-Top.png) -->
<!-- ![FLE-V1.0-3D](images/FLE-V1.0-3D.png) -->

<img src="images/FLE-V1.0-Top.png" width="800" />

<img src="images/FLE-V1.0-3D.png" width="800" />

---

## 📝 Fabrication

[Vue de la carte électronique](/FLE-V1.0/docs/FLE-V1.0-PCB-F_Cu.pdf), sérigraphie :

<img src="images/FLE-V1.0-Silkscreen.png" width="800" />

---

## 📝 BOM

| Référence  | Quantité | Valeur   | Description                            | Fournisseur                |
| :--------- | :------: | :------- | :------------------------------------- | :------------------------- |
| D1         |    1     | H        | LED Rouge, boitier CMS 0805            |                            |
| D2         |    1     | L        | LED Verte, boitier CMS 0805            |                            |
| D3         |    1     | HI       | LED Bleu, boitier CMS 0805             |                            |
| D4-D7      |    4     | 1N4148W  | boitier CMS SOD-123                    | [RS: 700-3671][RS-7003671] |
| J1         |    1     | Alim-EXT | connecteur coudé 2x2 au pas de 2.54 mm |                            |
| R1-R3      |    3     | 1k       | boitier CMS 0805                       |                            |
| R4-R6, R10 |    4     | 1M       | boitier CMS 0805                       |                            |
| R7         |    1     | 27k      | boitier CMS 0805                       |                            |
| R8         |    1     | 18k      | boitier CMS 0805                       |                            |
| R9         |    1     | 6k2      | boitier CMS 0805                       |                            |
| R11        |    1     | 560k     | boitier CMS 0805                       |                            |
| R12        |    1     | 5k6      | boitier CMS 0805                       |                            |
| TP1        |    1     | Sonde    | [Pointes de test à ressort][PTR]       | [RS: 261-5159][RS-2615159] |
| U1         |    1     | LM339    | [Comparateur différentiel][LM339]      | [RS: 661-2599][RS-6612599] |

---

## 👥 Contribuer au projet

Vous pouvez contribuer au projet en proposant des améliorations ou des corrections.

Pour cela, vous pouvez utiliser les issues ou les pull requests :

1. Fork it
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

Ou alors, vous pouvez nous contacter via le serveur Discord du groupe [FabTronic](https://discord.gg/aPnZ5Q7w6Q), dans le salon `#kicad`.

Nous pourrons vous ajouter sur le dépôt GitLab du projet, afin que vous aillez les droits sur <https://gitlab.com/fabtronic/>.

---

## Licence

[open source hardware](https://en.wikipedia.org/wiki/Open-source_hardware) (OSH)

<!-- Liens Internet -->
[LM339]: https://www.ti.com/product/LM339
[RS-6612599]: https://fr.rs-online.com/web/p/comparateurs/6612599
[RS-7003671]: https://fr.rs-online.com/web/p/diodes-de-commutation/7003671
[RS-2615159]: https://fr.rs-online.com/web/p/broches-de-test/2615159
[PTR]: https://www.traceparts.com/fr/search/rs-group-informatique-test-et-mesure-epi-et-hygiene-et-securite-test-mesure-connecteurs-de-test-pointes-de-test-a-ressort?CatalogPath=RS_COMPONENTS%3APSF_437278
